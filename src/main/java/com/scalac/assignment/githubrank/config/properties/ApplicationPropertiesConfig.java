package com.scalac.assignment.githubrank.config.properties;


import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(GithubApiProperties.class)
public class ApplicationPropertiesConfig {
}
