package com.scalac.assignment.githubrank.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@ConfigurationProperties("github-api")
@PropertySource("classpath:application.yml")
@Getter
@Setter
@Service
public class GithubApiProperties {

    private String host;
    private String GH_TOKEN;
    private String GH_USER;
}
