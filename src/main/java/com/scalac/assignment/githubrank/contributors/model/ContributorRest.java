package com.scalac.assignment.githubrank.contributors.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ContributorRest {

    private final String name;
    private final Long contributions;
}
