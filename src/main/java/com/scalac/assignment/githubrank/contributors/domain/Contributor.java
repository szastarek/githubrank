package com.scalac.assignment.githubrank.contributors.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Contributor {

    private final Long id;
    private final String login;
    private final Long contributions;
}
