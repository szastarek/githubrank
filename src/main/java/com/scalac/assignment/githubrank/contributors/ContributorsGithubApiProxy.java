package com.scalac.assignment.githubrank.contributors;

import com.scalac.assignment.githubrank.config.properties.GithubApiProperties;
import com.scalac.assignment.githubrank.contributors.domain.Contributor;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class ContributorsGithubApiProxy {

    private final GithubApiProperties githubApiProperties;
    private final RestTemplate restTemplate;

    public List<Contributor> getContributors(Repository repository) {
        var url = String.format("%s/repos/%s/contributors", githubApiProperties.getHost(), repository.getFullName());
        var responseEntity = restTemplate.getForEntity(url, Contributor[].class);
        return Arrays.asList(responseEntity.getBody());
    }
}
