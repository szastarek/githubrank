package com.scalac.assignment.githubrank.contributors.domain;

import com.scalac.assignment.githubrank.contributors.ContributorsGetService;
import com.scalac.assignment.githubrank.contributors.model.ContributorRest;
import com.scalac.assignment.githubrank.utils.Page;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class ContributorController {

    private final ContributorsGetService contributorsGetService;

    @GetMapping("/org/{organization}/contributors")
    public List<ContributorRest> getContributors(@PathVariable String organization, Page page) {
        return contributorsGetService.getContributors(organization, page);
    }
}
