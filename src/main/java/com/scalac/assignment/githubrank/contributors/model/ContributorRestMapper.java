package com.scalac.assignment.githubrank.contributors.model;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ContributorRestMapper {

    public List<ContributorRest> mapToRest(Map<String, Long> contributors) {
        return contributors
                .entrySet()
                .stream()
                .map(this::createContributorRest)
                .collect(Collectors.toList());
    }

    private ContributorRest createContributorRest(Map.Entry<String, Long> e) {
        return new ContributorRest(e.getKey(), e.getValue());
    }
}
