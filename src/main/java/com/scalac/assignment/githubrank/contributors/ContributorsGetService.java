package com.scalac.assignment.githubrank.contributors;

import com.scalac.assignment.githubrank.contributors.domain.Contributor;
import com.scalac.assignment.githubrank.contributors.model.ContributorRest;
import com.scalac.assignment.githubrank.contributors.model.ContributorRestMapper;
import com.scalac.assignment.githubrank.repositories.RepositoryGetService;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import com.scalac.assignment.githubrank.utils.Page;
import com.scalac.assignment.githubrank.utils.PageCreator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Service
@AllArgsConstructor
public class ContributorsGetService {

    private final ContributorsGithubApiProxy contributorsGithubApiProxy;
    private final RepositoryGetService repositoryGetService;
    private final ContributorRestMapper contributorRestMapper;

    public List<ContributorRest> getContributors(String organization, Page page) {
        var repositories = repositoryGetService.getOrganizationRepositories(organization);
        var contributors = getContributors(repositories);
        var contributorRests = contributorRestMapper.mapToRest(contributors).stream()
                .sorted(Comparator.comparingLong(ContributorRest::getContributions).reversed())
                .collect(toList());
        return PageCreator.create(contributorRests, page);
    }

    public List<Contributor> getContributors(Repository repository) {
        return contributorsGithubApiProxy.getContributors(repository);
    }

    private Map<String, Long> getContributors(List<Repository> repositories) {
        return repositories.stream()
                .map(this::getContributors)
                .flatMap(Collection::stream)
                .collect(groupingBy(Contributor::getLogin, summingLong(Contributor::getContributions)));
    }
}
