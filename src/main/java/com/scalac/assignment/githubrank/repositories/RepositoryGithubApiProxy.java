package com.scalac.assignment.githubrank.repositories;

import com.scalac.assignment.githubrank.config.properties.GithubApiProperties;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class RepositoryGithubApiProxy {

    private final GithubApiProperties githubApiProperties;
    private final RestTemplate restTemplate;

    public List<Repository> getOrganizationRepositories(String organization) {
        var url = String.format("%s/orgs/{organization}/repos", githubApiProperties.getHost());
        var responseEntity = restTemplate.getForEntity(url, Repository[].class, organization);
        return Arrays.asList(responseEntity.getBody());
    }
}
