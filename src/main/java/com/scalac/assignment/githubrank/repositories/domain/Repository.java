package com.scalac.assignment.githubrank.repositories.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Repository {

    private final Long id;
    private final String name;
    @JsonProperty(value = "full_name")
    private final String fullName;
    private final boolean isPrivate;
}
