package com.scalac.assignment.githubrank.repositories;

import com.scalac.assignment.githubrank.repositories.domain.Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RepositoryGetService {

    private final RepositoryGithubApiProxy repositoryGithubApiProxy;

    public List<Repository> getOrganizationRepositories(String organization) {
        return repositoryGithubApiProxy.getOrganizationRepositories(organization);
    }
}
