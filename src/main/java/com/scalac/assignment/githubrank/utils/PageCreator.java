package com.scalac.assignment.githubrank.utils;

import org.springframework.beans.support.PagedListHolder;

import java.util.List;

public class PageCreator {

    public static <T> List<T> create(List<T> collection, Page page) {
        var pagedListHolder = new PagedListHolder<T>(collection);
        pagedListHolder.setPage(page.getCurrentPage());
        pagedListHolder.setPageSize(page.getItemsOnPage());
        return pagedListHolder.getPageList();
    }
}
