package com.scalac.assignment.githubrank.utils;

import com.scalac.assignment.githubrank.config.properties.GithubApiProperties;
import lombok.AllArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class RestTemplateConfig {

    private final GithubApiProperties githubApiProperties;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .basicAuthentication(githubApiProperties.getGH_USER(), githubApiProperties.getGH_TOKEN())
                .build();
    }
}
