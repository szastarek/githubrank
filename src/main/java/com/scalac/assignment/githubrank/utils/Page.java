package com.scalac.assignment.githubrank.utils;

import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class Page {

    private final Integer currentPage;
    private final Integer itemsOnPage;

    public Integer getCurrentPage() {
        return Optional.ofNullable(currentPage).orElse(0);
    }

    public Integer getItemsOnPage() {
        return Optional.ofNullable(itemsOnPage).orElse(20);
    }
}
