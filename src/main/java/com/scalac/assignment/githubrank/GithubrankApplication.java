package com.scalac.assignment.githubrank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubrankApplication {

    public static void main(String[] args) {
        SpringApplication.run(GithubrankApplication.class, args);
    }

}
