package com.scalac.assignment.githubrank.repositories;

import com.scalac.assignment.githubrank.config.GithubRankTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@GithubRankTest
class RepositoryGithubApiProxyTestIT {

    private static final String ORGANIZATION = "octokit";
    private static final String ORGANIZATION_NOT_FOUND = "notFound";

    @Autowired
    private RepositoryGithubApiProxy repositoryGithubApiProxy;

    @Test
    @DisplayName("Should return list of repositories per organization")
    public void shouldReturnListOfRepositoriesPerOrganization() {
        //arrange && act
        var result = repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION);

        //assert
        assertThat(result).isNotEmpty();
    }

    @Test
    @DisplayName("Should throw exception when repositories github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION_NOT_FOUND));
    }
}