package com.scalac.assignment.githubrank.repositories;

import com.scalac.assignment.githubrank.config.properties.GithubApiProperties;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RepositoryGithubApiProxyTest {

    private static final String ORGANIZATION = "octokit";
    private static final String ORGANIZATION_NOT_FOUND = "notFound";
    private static final String HOST = "https://api.github.com/";

    @Mock
    private GithubApiProperties githubApiProperties;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private RepositoryGithubApiProxy repositoryGithubApiProxy;

    @Test
    @DisplayName("Should return list of repositories")
    public void shouldReturnListOfRepositories() {
        //arrange
        var repositories = List.of(new Repository(1L, "name_1", "full_name_1", false),
                new Repository(2L, "name_2", "full_name_2", false),
                new Repository(3L, "name_3", "full_name_3", true)).toArray(new Repository[0]);
        when(githubApiProperties.getHost()).thenReturn(HOST);
        when(restTemplate.getForEntity(anyString(), any(), anyString())).thenReturn(new ResponseEntity(repositories, HttpStatus.OK));

        //act
        var result = repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION);

        //assert
        assertThat(result).isNotEmpty();
        assertThat(result).usingFieldByFieldElementComparator().containsAll(Arrays.asList(repositories));
    }

    @Test
    @DisplayName("Should throw exception when repositories github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //arrange
        when(githubApiProperties.getHost()).thenReturn(HOST);
        when(restTemplate.getForEntity(anyString(), any(), anyString())).thenThrow(HttpClientErrorException.NotFound.class);

        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION_NOT_FOUND));
    }

}