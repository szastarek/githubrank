package com.scalac.assignment.githubrank.repositories;

import com.scalac.assignment.githubrank.repositories.domain.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RepositoryGetServiceTest {

    private static final String ORGANIZATION = "octokit";
    private static final String ORGANIZATION_NOT_FOUND = "notFound";

    @Mock
    private RepositoryGithubApiProxy repositoryGithubApiProxy;
    @InjectMocks
    private RepositoryGetService repositoryGetService;

    @Test
    @DisplayName("Should return list of repositories")
    public void shouldReturnListOfRepositories() {
        //arrange
        var repositories = List.of(new Repository(1L, "name_1", "full_name_1", false),
                new Repository(2L, "name_2", "full_name_2", false),
                new Repository(3L, "name_3", "full_name_3", true));
        when(repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION)).thenReturn(repositories);

        //act
        var result = repositoryGetService.getOrganizationRepositories(ORGANIZATION);

        //assert
        assertThat(result).usingFieldByFieldElementComparator().containsAll(repositories);
    }

    @Test
    @DisplayName("Should throw exception when repositories github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //arrange
        when(repositoryGithubApiProxy.getOrganizationRepositories(ORGANIZATION_NOT_FOUND)).thenThrow(HttpClientErrorException.NotFound.class);
        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> repositoryGetService.getOrganizationRepositories(ORGANIZATION_NOT_FOUND));
    }
}