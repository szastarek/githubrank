package com.scalac.assignment.githubrank.contributors.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ContributorRestMapperTest {

    @InjectMocks
    private ContributorRestMapper contributorRestMapper;

    @Test
    @DisplayName("Should map to rest")
    public void shouldMapToRest() {
        //arrange
        var contributorRests = Map.of("name_1", 10L, "name_2", 20L, "name_3", 30L);

        //act
        var result = contributorRestMapper.mapToRest(contributorRests);

        //assert
        assertThat(result).hasSize(3);
        assertThat(result).usingFieldByFieldElementComparator().contains(new ContributorRest("name_1", contributorRests.get("name_1")));
        assertThat(result).usingFieldByFieldElementComparator().contains(new ContributorRest("name_2", contributorRests.get("name_2")));
        assertThat(result).usingFieldByFieldElementComparator().contains(new ContributorRest("name_3", contributorRests.get("name_3")));
    }

    @Test
    @DisplayName("Should return empty list when map is empty")
    public void shouldReturnEmptyListWhenMapIsEmpty() {
        //arrange
        Map<String, Long> contributorRests = Map.of();

        //act
        var result = contributorRestMapper.mapToRest(contributorRests);

        //assert
        assertThat(result).isEmpty();
    }

}