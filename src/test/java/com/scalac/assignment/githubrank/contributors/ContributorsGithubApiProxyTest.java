package com.scalac.assignment.githubrank.contributors;

import com.scalac.assignment.githubrank.config.properties.GithubApiProperties;
import com.scalac.assignment.githubrank.contributors.domain.Contributor;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContributorsGithubApiProxyTest {

    private static final String REPO_FULL_NAME = "octokit/octokit.rb";
    private static final String REPO_FULL_NAME_NOT_FOUND = "octokit/not_found";
    private static final String HOST = "https://api.github.com/";

    @Mock
    private GithubApiProperties githubApiProperties;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private ContributorsGithubApiProxy contributorsGithubApiProxy;

    @Test
    @DisplayName("Should return list of contributors")
    public void shouldReturnListOfContributors() {
        //arrange
        var repository = new Repository(1L, "name", REPO_FULL_NAME, false);

        var contributors = List.of(new Contributor(13L, "login_3", 103L),
                new Contributor(14L, "login_4", 104L),
                new Contributor(15L, "login_5", 105L)).toArray(new Contributor[0]);
        when(githubApiProperties.getHost()).thenReturn(HOST);
        when(restTemplate.getForEntity(anyString(), any())).thenReturn(new ResponseEntity(contributors, HttpStatus.OK));

        //act
        var result = contributorsGithubApiProxy.getContributors(repository);

        //assert
        assertThat(result).isNotEmpty();
    }

    @Test
    @DisplayName("Should throw exception when contributors github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //arrange
        var repository = new Repository(1L, "name", REPO_FULL_NAME_NOT_FOUND, false);
        when(githubApiProperties.getHost()).thenReturn(HOST);
        when(restTemplate.getForEntity(anyString(), any())).thenThrow(HttpClientErrorException.NotFound.class);

        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> contributorsGithubApiProxy.getContributors(repository));
    }

}