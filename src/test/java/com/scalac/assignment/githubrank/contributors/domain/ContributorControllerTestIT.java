package com.scalac.assignment.githubrank.contributors.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scalac.assignment.githubrank.config.GithubRankTest;
import com.scalac.assignment.githubrank.contributors.model.ContributorRest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@GithubRankTest
class ContributorControllerTestIT {

    private static final String ORGANIZATION = "octokit";
    private static final String ORGANIZATION_NOT_FOUND = "notFound";

    @LocalServerPort
    private int port;

    @Test
    @DisplayName("Should return list of contributors ordered by contributions without pagination parameters")
    public void shouldReturnListOfContributorsOrderedByContributionsWithoutPaginationParameters() throws IOException {

        var response = given()
                .port(port)
                .contentType(ContentType.JSON)
                .when()
                .pathParam("organization", ORGANIZATION)
                .get("/org/{organization}/contributors")
                .then()
                .statusCode(200)
                .extract()
                .response();

        var mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var list = (List<ContributorRest>) mapper.readValue(response.asByteArray(), new TypeReference<List<ContributorRest>>() {
        });

        assertThat(list).isNotEmpty();
        assertThat(list).hasSize(20);
        assertThat(list).isSortedAccordingTo(Comparator.comparingLong(ContributorRest::getContributions).reversed());
    }

    @Test
    @DisplayName("Should return list of contributors ordered by contributions with pagination parameters")
    public void shouldReturnListOfContributorsOrderedByContributionsWithPaginationParameters() throws IOException {

        var response = given()
                .port(port)
                .contentType(ContentType.JSON)
                .when()
                .param("currentPage", 0)
                .param("itemsOnPage", 100)
                .pathParam("organization", ORGANIZATION)
                .get("/org/{organization}/contributors")
                .then()
                .statusCode(200)
                .extract()
                .response();

        var mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var list = (List<ContributorRest>) mapper.readValue(response.asByteArray(), new TypeReference<List<ContributorRest>>() {
        });

        assertThat(list).isNotEmpty();
        assertThat(list).hasSize(100);
        assertThat(list).isSortedAccordingTo(Comparator.comparingLong(ContributorRest::getContributions).reversed());
    }

    @Test
    @DisplayName("Should return second page of contributors ordered by contributions with pagination parameters")
    public void shouldReturnSecondPageOfContributorsOrderedByContributionsWithPaginationParameters() throws IOException {

        var firstPageResponse = given()
                .port(port)
                .contentType(ContentType.JSON)
                .when()
                .param("currentPage", 0)
                .param("itemsOnPage", 30)
                .pathParam("organization", ORGANIZATION)
                .get("/org/{organization}/contributors")
                .then()
                .statusCode(200)
                .extract()
                .response();

        var secondPageResponse = given()
                .port(port)
                .contentType(ContentType.JSON)
                .when()
                .param("currentPage", 1)
                .param("itemsOnPage", 30)
                .pathParam("organization", ORGANIZATION)
                .get("/org/{organization}/contributors")
                .then()
                .statusCode(200)
                .extract()
                .response();

        var mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var firstPage = (List<ContributorRest>) mapper.readValue(firstPageResponse.asByteArray(), new TypeReference<List<ContributorRest>>() {
        });
        var secondPage = (List<ContributorRest>) mapper.readValue(secondPageResponse.asByteArray(), new TypeReference<List<ContributorRest>>() {
        });

        assertAll("secondPage",
                () -> assertThat(secondPage).isNotEmpty(),
                () -> assertThat(secondPage).hasSize(30),
                () -> assertThat(secondPage).doesNotContainAnyElementsOf(firstPage),
                () -> assertThat(secondPage).isSortedAccordingTo(Comparator.comparingLong(ContributorRest::getContributions).reversed()),
                () -> assertThat(secondPage.get(0).getContributions()).isLessThanOrEqualTo(firstPage.get(29).getContributions()));
    }

    @Test
    @DisplayName("Should return 204 no content when organization not found")
    public void shouldReturnNoContentWhenOrganizationNotFound() {

        given()
                .port(port)
                .contentType(ContentType.JSON)
                .when()
                .pathParam("organization", ORGANIZATION_NOT_FOUND)
                .get("/org/{organization}/contributors")
                .then()
                .statusCode(204);
    }
}