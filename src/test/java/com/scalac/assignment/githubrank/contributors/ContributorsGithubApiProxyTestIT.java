package com.scalac.assignment.githubrank.contributors;

import com.scalac.assignment.githubrank.config.GithubRankTest;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@GithubRankTest
class ContributorsGithubApiProxyTestIT {

    private static final String REPO_FULL_NAME = "octokit/octokit.rb";
    private static final String REPO_FULL_NAME_NOT_FOUND = "octokit/not_found";

    @Autowired
    private ContributorsGithubApiProxy contributorsGithubApiProxy;

    @Test
    @DisplayName("Should return list of contributors")
    public void shouldReturnListOfContributors() {
        //arrange
        var repository = new Repository(1L, "name", REPO_FULL_NAME, false);

        //act
        var result = contributorsGithubApiProxy.getContributors(repository);

        //assert
        assertThat(result).isNotEmpty();
    }

    @Test
    @DisplayName("Should throw exception when contributors github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //arrange
        var repository = new Repository(1L, "name", REPO_FULL_NAME_NOT_FOUND, false);

        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> contributorsGithubApiProxy.getContributors(repository));
    }

}