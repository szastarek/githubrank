package com.scalac.assignment.githubrank.contributors;

import com.scalac.assignment.githubrank.contributors.domain.Contributor;
import com.scalac.assignment.githubrank.contributors.model.ContributorRest;
import com.scalac.assignment.githubrank.contributors.model.ContributorRestMapper;
import com.scalac.assignment.githubrank.repositories.RepositoryGetService;
import com.scalac.assignment.githubrank.repositories.domain.Repository;
import com.scalac.assignment.githubrank.utils.Page;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContributorsGetServiceTest {

    private static final String ORGANIZATION = "octokit";
    private static final String REPO_FULL_NAME = "octokit/octokit.rb";
    private static final String REPO_FULL_NAME_NOT_FOUND = "octokit/not_found";

    @Mock
    private ContributorsGithubApiProxy contributorsGithubApiProxy;
    @Mock
    private RepositoryGetService repositoryGetService;
    @Mock
    private ContributorRestMapper contributorRestMapper;
    @InjectMocks
    private ContributorsGetService contributorsGetService;

    @Test
    @DisplayName("Should return list of contributors")
    public void shouldReturnListOfContributors() {
        //arrange
        var repository = new Repository(1L, "name_1", REPO_FULL_NAME, false);
        var contributors = List.of(new Contributor(1L, "login_1", 100L),
                new Contributor(2L, "login_2", 102L),
                new Contributor(3L, "login_3", 103L));
        when(contributorsGithubApiProxy.getContributors(repository)).thenReturn(contributors);

        //act
        var result = contributorsGetService.getContributors(repository);

        //assert
        assertThat(result).usingFieldByFieldElementComparator().containsAll(contributors);
    }

    @Test
    @DisplayName("Should throw exception when repositories github api returns 404")
    public void shouldThrowExceptionWhen404() {
        //arrange
        var repository = new Repository(1L, "name_1", REPO_FULL_NAME_NOT_FOUND, false);
        when(contributorsGithubApiProxy.getContributors(repository)).thenThrow(HttpClientErrorException.NotFound.class);

        //act && assert
        assertThrows(HttpClientErrorException.NotFound.class, () -> contributorsGetService.getContributors(repository));
    }

    @Test
    @DisplayName("Should return list of contributors by organization name")
    public void shouldReturnListOfContributorsByOrganizationName() {
        //arrange
        var page = new Page(0, 30);
        var repositories = List.of(new Repository(1L, "name_1", "full_name_1", false),
                new Repository(1L, "name_2", "full_name_2", false));
        var contributorsFirstRepo = List.of(new Contributor(10L, "login_1", 100L),
                new Contributor(12L, "login_2", 102L));
        var contributorsSecondRepo = List.of(new Contributor(13L, "login_3", 103L),
                new Contributor(14L, "login_4", 104L),
                new Contributor(15L, "login_5", 105L));
        when(repositoryGetService.getOrganizationRepositories(ORGANIZATION)).thenReturn(repositories);
        when(contributorsGithubApiProxy.getContributors(repositories.get(0))).thenReturn(contributorsFirstRepo);
        when(contributorsGithubApiProxy.getContributors(repositories.get(1))).thenReturn(contributorsSecondRepo);
        when(contributorRestMapper.mapToRest(anyMap())).thenCallRealMethod();

        //act
        var result = contributorsGetService.getContributors(ORGANIZATION, page);

        //assert
        assertThat(result).hasSize(5);
        assertThat(result).isSortedAccordingTo(Comparator.comparingLong(ContributorRest::getContributions).reversed());
    }
}