FROM openjdk:12
ADD build/libs/githubrank-0.0.1-SNAPSHOT.jar app.jar
ENV GH_TOKEN=secret
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/app.jar"]